﻿using System;
using System.Text; //for sb

namespace base64toascii
{
    class Program
    {
        static void Main(string[] args)
        {
           System.Console.WriteLine("Enter base64 string: ");
           string input = Console.ReadLine();
           //stringbuilder declaration
           StringBuilder base64 = new StringBuilder(input);
           //code to ensure you're receiving base64 encoded string
           try
           {
            if(base64.ToString().Contains("="))
            {
                Console.WriteLine(Decode(base64));
            }
            else
            {
                Console.WriteLine(Decode(base64.Append("=")));
            }               
           }
           catch (FormatException)
           {
             Console.WriteLine($"Not a base64 encoded string");
           }
        }

        public static string Decode(StringBuilder base64data){
            var base64EncodedBytes = System.Convert.FromBase64String(base64data.ToString());
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
